package models


trait Command {
  override def toString: String = this.getClass.getName
}

trait CommandSupByVal extends Command {
var variable : String
///var variable : Int
override def toString: String = this.getClass.getName + "(" + variable + ") "
}

class CommandClasses {

}

case class PUSHINT (var variable : String) extends CommandSupByVal
{
  override def toString: String = "PUSHINT " + variable + " "
}
case class PUSH (var variable : String) extends CommandSupByVal
{
  override def toString: String = "PUSH " + variable + " "
}
case class SLIDE (var variable : String) extends CommandSupByVal
{
  override def toString: String = "SLIDE " + variable + " "
}
case class ALLOC (var variable : String) extends CommandSupByVal
{
  override def toString: String = "ALLOC " + variable + " "
}
case class UPDATE (var variable : String) extends CommandSupByVal
{
  override def toString: String = "UPDATE " + variable + " "
}
case class MKAP() extends Command
{
  override def toString: String = "MKAP " + " "
}
case class MKINT() extends Command
{
  override def toString: String = "MKINT " + " "
}
case class GET () extends Command
{
  override def toString: String = "GET "+ " "
}
///case class_ => {
