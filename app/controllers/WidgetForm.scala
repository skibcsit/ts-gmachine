package controllers

object WidgetForm {
  import play.api.data.Forms._
  import play.api.data.Form


  case class Data(command: String, stack: Option[String], graf: Option[String])

  val form = Form(
    mapping(
      "command" -> nonEmptyText,
      "stack" -> optional(text), ////скорее всего нужно удалить
      "graf" -> optional(text),   ////скорее всего нужно удалить
    )(Data.apply)(Data.unapply)
  )
}
