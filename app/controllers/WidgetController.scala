package controllers

import javax.inject.Inject
import models.Widget
import play.api.data._
import play.api.i18n._
import play.api.mvc._
import actions._
import java.io.IOException
//import java.nio.file.Files
//import java.nio.file.Path
//import java.nio.file.Paths
import java.util

import play.api.libs.json._
import play.api.libs.functional.syntax._

import scala.concurrent.ExecutionContext



class WidgetController @Inject()(cc: MessagesControllerComponents)  (implicit webJarsUtil: org.webjars.play.WebJarsUtil) extends MessagesAbstractController(cc) {
  import WidgetForm._

  var stackString = ""

  var stackList: List[List[String]] = Nil
  var grafList: List[String] = Nil
  var commandCount: Int = 0
  var commandList:  List[String] = Nil

  var gMachineController: GMachineController = new GMachineController()
  var parser : Parser = new Parser(gMachineController)
  Counter.setZero
  JEdge.clear()
  JPair.clear()
  JNode.clear()
  gMachineController.cleanMachine(parser)
  gMachineController.stackClean



  private var widget = Widget("","","")

  private val postUrl = routes.WidgetController.createWidget()

  def index = Action {
    Ok(views.html.index())
  }

  def listWidgets = Action { implicit request: MessagesRequest[AnyContent] =>


    Ok(views.html.listWidgets(commandList,widget,grafList,stackList, commandCount, form, postUrl))
  }

  // This will be the action that handles our form post
  def createWidget = Action { implicit request: MessagesRequest[AnyContent] =>
    val errorFunction = { formWithErrors: Form[Data] =>
      // This is the bad case, where the form had validation errors.
      // Let's show the user the form again, with the errors highlighted.
      // Note how we pass the form with errors to the template.
      BadRequest(views.html.listWidgets(commandList,widget,grafList,stackList, commandCount, formWithErrors, postUrl))
    }

    val successFunction = { data: Data =>
      stackList = Nil
      grafList = Nil
      commandCount = 0
      commandList = Nil

      var gMachineController: GMachineController = new GMachineController()
      var parser : Parser = new Parser(gMachineController)
      /// var counter: Counter = new Counter
      parser.setInstructions(data.command)
      parser.parse
      parser.call
      Counter.setZero
      stackString =   gMachineController.stackString
      var jList =   gMachineController.jList
      commandCount = jList.length
      for (command <- parser.commandList)
      {
        commandList = commandList :+ command.toString
      }

      stackList = gMachineController.stackList

      /*val pair = JPair (JNode.list, JEdge.list)
       val json = Json.toJson(pair)*/
      JEdge.clear()
      JPair.clear()
      JNode.clear()
      for (jPair <- jList)
      {
        var jElem = Json.toJson(jPair)
        grafList = grafList :+ jElem.toString()
      }/////Для каждого элемента*/
      for (jPair <- jList)
      {
        var jElem = Json.toJson(jPair)
        grafList = grafList :+ jElem.toString()
      }


      gMachineController.cleanMachine(parser)
      gMachineController.stackClean
      this.widget = new Widget(command = data.command, stack = stackString, graf = "Граф")
      Redirect(routes.WidgetController.listWidgets()).flashing("info" -> "Запущена команда:")
    }

    val formValidationResult = form.bindFromRequest
    formValidationResult.fold(errorFunction, successFunction)
  }
  implicit val nodeWrites: Writes[JNode] = (
    (JsPath \ "id").write[String] and
      (JsPath \ "label").write[String] and
      (JsPath \ "x").write[Int] and
      (JsPath \ "y").write[Int] and
      (JsPath \ "size").write[Int]
    )(unlift(JNode.unapply))

  implicit val edgeWrites: Writes[JEdge] = (
    (JsPath \ "id").write[String] and
      (JsPath \ "source").write[String] and
      (JsPath \ "target").write[String]
    )(unlift(JEdge.unapply))

  implicit val pairWrites: Writes[JPair] = (
    (JsPath \ "nodes").write[List[JNode]] and
      (JsPath \ "edges").write[List[JEdge]]
    )(unlift(JPair.unapply))


  def answerJSON = Action {
    val pair = JPair (JNode.list, JEdge.list)
    val json = Json.toJson(pair)
    Ok(json)
  }

}
