package actions


class Stack {
  var list: List[DataNode] = Nil

  def push(x: DataNode): Unit = {
    this.list = x :: this.list
  }

  def push(x: String): Unit = {
    var node: ValueNode = new ValueNode(x)
    this.list = node :: this.list
  }

  def getByNum(x: Int): DataNode = this.list(this.list.length - x) /// возвращат x'тый элемент
  def isEmpty(): Boolean = this.list.isEmpty

  def length(): Int = this.list.length

  def top(): DataNode = this.list.head

  def pop(): Unit = {
    this.list = this.list.tail
  }

  def pop(n: Integer): Unit = {
    this.list = this.list.dropRight(n)
  }

  def PUSHINT(c: String): Unit = push(c)

  def PUSH(m: Integer): Unit = push(this.list(m))

  def SLIDE(m: Integer): Unit = ///Копирует УВС в элемент (length-m) стека
  {
    this.list = this.list.updated(this.list.length - m, top)
    pop
  }

  override def toString(): String = {
    var str: String = "-> [ "
    for (i <- this.list) {
      str = str + i + "; "
    }
    str + " ]"
  }

  def ALLOC(m: Int): Unit = /// Создает m новых вершин, вставляет в них топ
  {
    for (i <- 1 to m) {
      var newNode: DataNode = new ValueNode("null") {}
      push(newNode)
    }


  }
}

