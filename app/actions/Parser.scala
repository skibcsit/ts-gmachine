package actions

///import java.nio.file.{Files, Paths, StandardOpenOption}
import java.nio.charset.StandardCharsets

import models.{CommandSupByVal, MKAP, MKINT, PUSH, PUSHINT, UPDATE, _}

class   Parser (gMachineController: GMachineController) {
  var instructions: String = ""

  def setInstructions(newStr: String): Unit =
    this.instructions = newStr

  var commandList: List[Command] = Nil
  var iteration: Int = 0

  def iterate(command : Command) = {
    gMachineController.addToStackString(iteration + ": " + command.toString)   ///////сделать так чтобы можно было разделить строки
    gMachineController.makeJSONFromStack(iteration) ///////сделать так чтобы можно было разделить строки
    gMachineController.addToJList(JPair (JNode.list, JEdge.list))
    gMachineController.saveStackToStackList
    JEdge.clear()
    JPair.clear()
    JNode.clear()
    this.iteration = iteration + 1
  }

  def parse: Unit = {
    var strArr = instructions.split("\\s+")
    var i = 0
    while (i < strArr.length) {
      strArr(i) match {
        case "PUSHINT" => {
          i = i + 1
          commandList = this.commandList :+ PUSHINT(strArr(i))
        }
        case "PUSH" => {
          i = i + 1
          if (strArr(i) forall Character.isDigit) {
            commandList = this.commandList :+ PUSH(strArr(i))
          }
        }
        case "SLIDE" => {
          i = i + 1
          if (strArr(i) forall Character.isDigit) {
            commandList = this.commandList :+ SLIDE(strArr(i))
          }
          else {
            print("Wrong parametr of macros")
          }
        }
        case "ALLOC" => {
          i = i + 1
          if (strArr(i) forall Character.isDigit) {
            commandList = this.commandList :+ ALLOC(strArr(i))
          }
          else {
            print("Wrong parametr of macros")
          }
        }
        case "UPDATE" => {
          i = i + 1
          if (strArr(i) forall Character.isDigit) {
            commandList = this.commandList :+ UPDATE(strArr(i))
          }
          else {
            print("Wrong parametr of macros")
          }
        }
        case "MKAP" => {
          commandList = this.commandList :+ MKAP()
        }
        case "MKINT" => {
          commandList = this.commandList :+ MKINT()
        }
        case "GET" => {
          commandList = this.commandList :+ GET()
        }
        case _ => {
          print("Wrong macros ")
          i = strArr.length - 1
        }

      }
      i = i + 1;
    }
  }

  def call: Unit = {
    var i = 0
    for (command <- commandList) command  match {
      case PUSHINT(n) => {
        gMachineController.PUSHINT(command.asInstanceOf[CommandSupByVal].variable)
        iterate(command)
      }
      case PUSH(n) => {
        gMachineController.PUSH(command.asInstanceOf[CommandSupByVal].variable.toInt)
        iterate(command)
      }
      case SLIDE(n) => {
        gMachineController.SLIDE(command.asInstanceOf[CommandSupByVal].variable.toInt)
        iterate(command)
      }
      case ALLOC(n) => {
        gMachineController.ALLOC(command.asInstanceOf[CommandSupByVal].variable.toInt)
        iterate(command)
      }
      case UPDATE(n) =>
        {
          gMachineController.UPDATE(command.asInstanceOf[CommandSupByVal].variable.toInt)
          iterate(command)
        }

      case MKAP() => {
        gMachineController.MKAP()
        iterate(command)
      }
      case MKINT() => {
        gMachineController.MKINT()
        iterate(command)
      }
      case GET() => {
        gMachineController.GET()
        iterate(command)
      }
    }
  }
}

/*
    while (i < strArr.length) {
      strArr(i) match {
        case "PUSHINT" => {
          i = i + 1
          gMachineController.PUSHINT(strArr(i))
          gMachineController.addToStackString(iteration + ": " + "PUSHINT")
          gMachineController.makeJSONFromStack(iteration)
          iterate
        }
        case "PUSH" => {
          i = i + 1
          if (strArr(i) forall Character.isDigit) {
            gMachineController.PUSH(strArr(i).toInt)
          }
          else {
            print("Wrong parameter of macros")
          }
          gMachineController.addToStackString(iteration + ": " + "PUSH")
          gMachineController.makeJSONFromStack(iteration)
          iterate
        }
        case "SLIDE" => {
          i = i + 1
          if (strArr(i) forall Character.isDigit) {
            gMachineController.SLIDE(strArr(i).toInt)
          }
          else {
            print("Wrong parametr of macros")
          }
          gMachineController.addToStackString(iteration + ": " + "SLIDE")
          gMachineController.makeJSONFromStack(iteration)
          iterate
        }
        case "ALLOC" => {
          i = i + 1
          if (strArr(i) forall Character.isDigit) {
            gMachineController.ALLOC(strArr(i).toInt)
          }
          else {
            print("Wrong parametr of macros")
          }
          gMachineController.addToStackString(iteration + ": " + "ALLOC")
          gMachineController.makeJSONFromStack(iteration)
          iterate
        }
        case "UPDATE" => {
          i = i + 1
          if (strArr(i) forall Character.isDigit) {
            gMachineController.UPDATE(strArr(i).toInt)
          }
          else {
            print("Wrong parametr of macros")
          }
          gMachineController.addToStackString(iteration + ": " + "UPDATE")
          gMachineController.makeJSONFromStack(iteration)
          iterate
        }
        case "MKAP" => {
          gMachineController.MKAP()
          gMachineController.addToStackString(iteration + ": " + "MKAP")
          gMachineController.makeJSONFromStack(iteration)
          iterate
        }
        case "MKINT" => {
          gMachineController.MKINT
          gMachineController.addToStackString(iteration + ": " + "MKINT")
          gMachineController.makeJSONFromStack(iteration)
          iterate
        }
        case "GET" => {
          gMachineController.GET
          gMachineController.addToStackString(iteration + ": " + "GET")
          gMachineController.makeJSONFromStack(iteration)
          iterate
        }
        case _ => {
          print("Wrong macros ")
          i = strArr.length - 1
        }

      }
      i = i + 1;
    }
  }*/