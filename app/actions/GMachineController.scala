package actions


object Counter {

  var id: Int = 0

  def iterate: Unit =
  {
    this.id=this.id + 1
    if (this.id > 10000) this.id = 0
  }
  def setZero: Unit = {
    this.id = 0
  }
}

class GMachineController {
  var jList: List[JPair] = Nil
  var stackList : List[List[String]] = Nil

  var stackString = new String
  var dump = new Stack
  var stack = new Stack
  //var counter: Counter = new Counter


  def addToJList(jPair: JPair): Unit = {
    jList = jList :+ jPair
  }
  def saveStackToStackList: Unit = {
    var list: List[String] = Nil
    for (elem <- stack.list){
      list = list :+ elem.toString
    }
    stackList = stackList :+ list
  }

  def stackClean: Unit  =
  {
    this.stack = new Stack
    this.stackString=""
  }

  def addToStackString(makros: String): Unit = {
    stackString =stackString +  makros + ": " + stack.toString() +'\n'
  }
  def addToStackString: Unit = {
    stackString = stackString + stack.toString() +'\n'
  }
  def PUSH (m: Int): Unit =
    {
      stack.PUSH(m)
    }
  def SLIDE (m: Int) : Unit = {
    stack.SLIDE(m)
  }

  /*
  *
  * Одна из проблем заключается в том
  * что
  * а) DataNode может быть как Alpha так и Value, а значит
  * match работает плохо
  *
  *
  *
  * */
  def UPDATE(m: Int): Unit = {
    stack.list(stack.list.length - m) match {
      case a: DataNode => {
        a.setLeft(stack.top().left)
        a.setRight(stack.top().right)
        a.setVal(stack.top().elem)
        stack.pop()
      }
      case _ => {
        print("UPDATE macros error")
      }
    }
  }
  def ALLOC (m: Int) : Unit = stack.ALLOC(m)
  def PUSHINT (c: String) : Unit =
    {
      stack.PUSHINT(c)
    }

  def MKAP (): Unit = {
    var left = stack.top
    var right =stack.getByNum(stack.length - 1)
    var newAlpha: DataNode = new AlpfaNode(left,right)
    stack.pop
    stack.pop
    stack.push(newAlpha)
  }

  def MKINT (): Unit = {
    var newCons: DataNode = new ValueNode(dump.top.toString)
    dump.pop
    stack.push(newCons)
  }

    def GET (): Unit = {
      dump.push(stack.top)
      stack.pop
    }
/*
  def EVAL (): Unit = {
    stack.top() match {
      case  ValueNode =>
      case  AlpfaNode =>
    }
  }
*/
  def UNWIND (): Unit = {
    dump.push(stack.top)
    stack.pop
  }

  def RET (m : Int): Unit = {
    dump.push(stack.top)
    stack.pop
  }


  def makeJSONFromStack(iteration: Int): Unit = {    ///////CHANGE
    for (i <- stack.list) {
      i match {
        case a: DataNode  if !JNode.existsInSubList(a, iteration) => {
          a.jsonOrder(JNode.maxX, JNode.maxY + 4, iteration)
          a.checkOff()
        }
        case b: DataNode  if JNode.existsInSubList(b, iteration) => {}
        case _ => print("Error in the stack element: " + i)
      }
    }
    JNode.save()  ///////CHANGE
  }

  def cleanMachine(parser: Parser): Unit = {
    JPair.clear()
    JNode.clear()
    JEdge.clear()
    parser.iteration = 0
    jList  = Nil
    stackList = Nil
    parser.commandList = Nil
    parser.instructions = ""
  }
}
