package actions



  case class JNode(id: String, label: String, x: Int, y: Int, size: Int = 3)

  case class JEdge(id: String, source: String, target: String)

  case class JPair(nodes: List[JNode], edges: List[JEdge])

  object JPair {
    def clear(): Unit = {
      this.nodes = Nil
      this.edges = Nil
    }

    var nodes: List[JNode] = JNode.list
    var edges: List[JEdge] = JEdge.list
  }

  object JNode {

    def maxX : Int = {     /////максимальный во временном листе
      var max : Int = 0

      for (i <- this.subList) if (i.x > max) max = i.x
      max + 4
    }
    def maxY : Int = {
      var max : Int = 0

      for (i <- this.list) if (i.y > max) max = i.y
      max
    }

    var list: List[JNode] = Nil
    var subList: List[JNode] = Nil

    def save() = {       ///////CHANGE
      list = list ::: subList
      subList =  Nil
    }

    def savetosubList(node: JNode) = {
      subList = subList ::: List(node)
    }
    def existsInSubList(node: DataNode, iteration: Int):Boolean = {
      for (i <- subList)
      {
        if (i.id == (iteration.toString + ":" + node.id.toString)) return true
      }
      false
    }
    def makeJObj (dataNode: DataNode , x : Int, y : Int, iteration: Int): JNode = {
     //// JNode(iteration.toString + ":" + dataNode.id.toString,dataNode.elem + ":  ["+iteration+"]",x,y)
     //// JNode(iteration.toString + ":" + dataNode.id.toString,dataNode.elem + ":  ["+iteration+"]",x,y)
      JNode(iteration.toString + ":" + dataNode.id.toString,dataNode.elem + "  id:" + dataNode.id,x,y)

    }
    def clear(): Unit = {
    this.list = Nil
    }
  }

  object JEdge {
   // var list: List[JEdge] = Nil
   def clear(): Unit = {
     this.list = Nil
   }
    var list: List[JEdge] = Nil
    def save(node: JEdge) = {
      list = list ::: List(node)
    }
    def makeJObj (dataNodeFrom: DataNode, dataNodeTo: DataNode, iteration: Int): JEdge = {
      JEdge(iteration.toString + ":" +dataNodeFrom.id.toString +" - " + dataNodeTo.id.toString, iteration.toString + ":" +dataNodeFrom.id.toString , iteration.toString + ":" +dataNodeTo.id.toString)
    }
  }

