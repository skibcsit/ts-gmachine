
name := """play-scala-forms-example"""

version := "1.0"

scalaVersion := "2.12.8"

lazy val root = (project in file(".")).enablePlugins(PlayScala, UniversalPlugin)

crossScalaVersions := Seq("2.11.12", "2.12.4")







val akkaVersion = "2.5.18"

libraryDependencies += guice
libraryDependencies += "org.webjars" %% "webjars-play" % "2.6.3"
libraryDependencies += "org.webjars" % "sigma.js" % "1.0.3"

libraryDependencies += "org.jsoup" % "jsoup" % "1.11.3"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"
libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % akkaVersion
libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test
libraryDependencies += "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % Test
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.1" % Test
libraryDependencies += "org.webjars" % "sigma.js" % "1.0.3"
libraryDependencies += "com.softwaremill.macwire" %% "macros" % "2.3.2" % "provided"

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-Xfatal-warnings"
)


///libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test