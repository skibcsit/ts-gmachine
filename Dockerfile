FROM openjdk:8
FROM hseeberger/scala-sbt:11.0.2_2.12.8_1.2.8




# Define working directory
WORKDIR /root
ENV PROJECT_HOME /usr/src

COPY ["build.sbt", "/tmp/build/"]
COPY ["project/plugins.sbt", "project/build.properties", "/tmp/build/project/"]
RUN cd /tmp/build && \
 sbt update && \
 sbt compile

RUN mkdir -p $PROJECT_HOME/app

WORKDIR $PROJECT_HOME/app

COPY . $PROJECT_HOME/app

RUN sbt universal:packageBin

# We are running play on this port so expose it
ENV PORT=80
EXPOSE 80

RUN unzip target/universal/play-scala-forms-example-1.0.zip

RUN chmod +x play-scala-forms-example-1.0/bin/play-scala-forms-example

ENTRYPOINT $PROJECT_HOME/app/play-scala-forms-example-1.0/bin/play-scala-forms-example